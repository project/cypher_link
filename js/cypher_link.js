/**
 * @file
 * JavaScript for Cypher Link.
 */

(function (base, factory, name) {
  'use strict';
  base[name] = factory();
  if (typeof define === 'function' && define.amd) {
    define(function() { return base[name]; });
  } else if (typeof exports === 'object') {
    module.exports = base[name];
  }
})((typeof window === 'object' && window) || this, function () {
  'use strict';

  var CypherLink = function () {};
  CypherLink.fn = CypherLink.prototype = {
    cypher: function(link) {
      var index, code, newLink;
      index = 0;
      code = 0;
      newLink = '';
      while (index < link.length) {
        code = link.charCodeAt(index);
        if (code >= 8364) {
          code = 128;
        }
        newLink += String.fromCharCode(code - 1);
        index++;
      }
      return newLink;
    }
  };

  var cypherLink = new CypherLink();

  /**
   * Decrypt link.
   */
  cypherLink.decrypt = function(encryptedLink) {
    location.href = this.cypher(encryptedLink);
  };

  return cypherLink;
}, 'cypherLink');
