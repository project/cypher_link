<?php

namespace Drupal\cypher_link\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'cypher_link' formatter.
 *
 * @FieldFormatter(
 *   id = "cypher_link",
 *   label = @Translation("Cypher Link"),
 *   field_types = {
 *     "email",
 *     "link",
 *     "integer",
 *     "string",
 *     "text"
 *   }
 * )
 */
class CypherLinkFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Declare settings 'scheme' & 'label', with a default value.
      'scheme' => '',
      'label' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['scheme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link Scheme'),
      '#description' => $this->t('Example, "mailto:" for mailto:<email>, "tel:" for tel<number> etc.'),
      '#default_value' => $this->getSetting('scheme'),
    ];
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link Label'),
      '#description' => $this->t('Example, "Contact Us", "Email Your Query" etc. (Leave empty to use field value as label).'),
      '#default_value' => $this->getSetting('label'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();

    if (isset($settings['label']) && trim($settings['label'])) {
      $summary[] = $this->t('A static label "@label" will be used instead of the field value.', ['@label' => $settings['label']]);
    }
    if (isset($settings['scheme']) && trim($settings['scheme'])) {
      $summary[] = $this->t('Applied scheme "@scheme" will be prefixed to the field value and cyphered.', ['@scheme' => $settings['scheme']]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        // '#type' => 'link', does NOT work with encrypted uri/url values.
        '#theme' => 'cypher_link',
        '#data' => $this->viewValue($item),
        '#attached' => ['library' => ['cypher_link/cypher_link']],
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $url = '';

    $settings = $this->getSettings();
    $title = trim($settings['label']);

    $field_type = $item->getFieldDefinition()->getType();
    $field_value = $item->getValue();

    if ($field_type == 'link') {
      if (!$title) {
        $title = $field_value['title'] ?? '';
      }
      $url = $field_value['uri'];
    }
    else {
      $url = $field_value['value'];
    }

    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    $string = nl2br(Html::escape(trim($settings['scheme']) . $url));

    // Encrypt the string characters, converting character to integer
    // and vice versa interpreting their byte-values.
    $encrypt = implode(
      '',
      array_map(
        function ($c) {
          return chr(ord($c) + 1);
        },
        str_split($string)
      )
    );

    return [
      'label' => $title ? $title : $encrypt,
      'url' => $encrypt,
    ];
  }

}
