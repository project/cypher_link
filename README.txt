CONTENTS OF THIS FILE
---------------------
- Introduction
- Installation
- Configuration
- Usage


INTRODUCTION
------------
Cypher Link module specifically targets BOTs' email-spamming issue.
BOTs read exposed 'mailto' Email IDs and spam individuals' inboxes.

At certain level, this module helps in overcoming email-spamming issue
by exposing encrypted 'mailto' Email IDs to the BOTs.

Here, concerned link items are encrypted at the server end (through PHP), and
Javascript on the client side decrypts them to take appropriate action.

Encryption and Decryption
of field values happens over 'Cypher Link' field-formatter.
And currently, it has support for Links, Email, Text and Integer field types.

Important:
Javascript should be enabled at the client end for the working of this module.


INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/documentation/install/modules-themes/modules-8


CONFIGURATION
-------------
Choose field-formatter 'Cypher Link' for your concerned field by navigating to-
Structure > Content Types > My Content Type > Manage Display

It also allows below additional Format-settings:
  - LINK SCHEME
    Here, you can specify Link Scheme to support your field values.
    Example, 'mailto:' for mailto:<email>, 'tel:' for tel:<number> etc.
    IMPORTANT: Applied scheme will be prefixed to the field value and cyphered.
  - LINK LABEL
    You can specify static Link Label to use instead of the field value.
    Example, 'Contact Us', 'Email Your Query' etc.

If you want to theme 'Cypher Link' of your choice.
Copy twig file cypher_link/templates/cypher-link.html.twig and
place it to your current theme's layout folder and edit accordingly.


USAGE
-------------
Here is an example considering an 'Email' field type.

We are trying expose a mailto encrypted anchor link identical to:
<a href="mailto:abhay.saraf@invalid.com">
  abhay.saraf@invalid.com
</a>

[CASE 1]
Field Value: abhay.saraf@invalid.com
Field Format: Cypher Link
Field Format Settings:
  - Link Scheme: 'mailto:'
  - Link Label:

Now, the Cypher Link formatter will output:
<a href="javascript:cypherLink.decrypt('nbjmup;bcibz/tbsbgAjowbmje/dpn');">
  nbjmup;bcibz/tbsbgAjowbmje/dpn
</a>

[CASE 2]
Field Value: abhay.saraf@invalid.com
Field Format: Cypher Link
Field Format Settings:
  - Link Scheme: 'mailto:'
  - Link Label: 'Contact Us'

Now, the Cypher Link formatter will output:
<a href="javascript:cypherLink.decrypt('nbjmup;bcibz/tbsbgAjowbmje/dpn');">
  'Contact Us'
</a>
